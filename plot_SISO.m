clear; close all; clc; 
addpath("/MATLAB Drive/Final-result-WuR-ZF/Database")
figure
%% 
% load('DB_WuR_1N_37alpha1Mw_2Border_10000R.mat','Prob_wu','TdB','alpha','Pt','Mw','lamda_b','Nt','NbIter','a') 
% plot(TdB,Prob_wu,'r','LineWidth',1.2);
% hold on
% [Pw, Pw_broadcast]=get_ProbWu_anal_exact(TdB,a,alpha,Pt,lamda_b,Mw); 
% plot(TdB,Pw,'c*','LineWidth',0.5); 
% Pw
% hold on
% plot(TdB,Pw_broadcast,'ko','LineWidth',1.2); 
%%
hold on
load('DB_WuR_1N_40alpha1Mw_2Border_10000R.mat','Prob_wu','TdB','alpha','Pt','Mw','lamda_b','Nt','NbIter','a') 
plot(TdB,Prob_wu,'r','LineWidth',1.2);
hold on
[Pw, Pw_broadcast]=get_ProbWu_anal_exact(TdB,a,alpha,Pt,lamda_b,Mw); 
plot(TdB,Pw,'b*','LineWidth',0.5); 
hold on
plot(TdB,Pw_broadcast,'ko','LineWidth',1.2); 
%%
hold on
load('DB_WuR_1N_44alpha1Mw_2Border_10000R.mat','Prob_wu','TdB','alpha','Pt','Mw','lamda_b','Nt','NbIter','a') 
plot(TdB,Prob_wu,'r','LineWidth',1.2);
hold on
[Pw, Pw_broadcast]=get_ProbWu_anal_exact(TdB,a,alpha,Pt,lamda_b,Mw); 
plot(TdB,Pw,'b*','LineWidth',0.5); 
hold on
plot(TdB,Pw_broadcast,'ko','LineWidth',1.2);

grid on

legend('Monte-carlo simulations','Analytical expression P_wu','Analytical expression P_{SISO}')
xlabel('Threshold in dB');ylabel('Wake-up Probability')
% title(['M_{w}=',num2str(Mw),', N_{t}=',num2str(Nt)])


