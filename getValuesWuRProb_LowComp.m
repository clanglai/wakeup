function getValuesWuRProb_LowComp(Nt,Mw,alpha,NbIter,ScnTh)
% Nt=2*Mw-1 : No. of transmit antennas
% Mw: No. of WSs per cell
% alpha: Path-loss exponent
% NbIter: No. of Monte-Carlo simulation runs
% ScnTh (scenario theorique)="onn" --> Update of Nt=2*Mw-1
% ScnTh="off" --> No update of Nt
if ScnTh=="onn"
    Nt=2*Mw-1;
elseif ScnTh=="off"
    Nt=Nt;
end
%% 
a=1; %RF-DC conversion efficiency
lamda_b=10.*(0.5^2.*pi)^-1; %: density of BSs #BSs/Km^2
Pt=10^4.6; %transmit power in mW
aP=a*Pt/Mw % Normalized transmit power between users
TdB=[-40:2:10]; % threshold in dB
T=10.^(TdB./10); 
WS0=[0 0]; %the test WS at the origin
Border=2;  %simulation border in Km
alpha,Mw,Nt,
for iter=1:NbIter
    clear BS;
    %% Generate the network centered at the origin
    %% Positions of BSs
    N_BS=poissrnd(lamda_b*Border^2);% No. of PPP BSs within the area 
    xb=Border*rand(1,N_BS);% x-coordinates of BSs                                                                                    %
    yb=Border*rand(1,N_BS);% y-coordinates of BSs                                                                                    %
    BS=[xb' yb'];% BSs
    BS=BS-Border/2; 
    BS=[BS [1:length(BS)]'];
    %% BSs-WS at [0 0] Channels 
    dist_BS_WS0=pdist2(WS0,BS(:,1:2));% distances [Km] from WS to all BSs
    [dist_BS_WS_min_0,B]= min(dist_BS_WS0);% Serving closest BS 
    serving=BS(B,:);
    ser_ind=serving(3); %BS0 assigned to WS0
    %% Generate the channel matrix 
    FC=sqrt(0.5)*(randn(Mw,Nt,N_BS) + 1i*randn(Mw,Nt,N_BS));% Rayleigh Fading channel (FC) % Channels between each BS and the Mw corresponding WSs
    %% ZF beamforming
    %% Desired signal at WS0 
    h0=FC(:,:,ser_ind); % WS0-BS0 channel 
    W_ZF_0=h0'/(h0*h0');% ZF 
    Pr_d=aP.*(norm(h0(1,:)*W_ZF_0(:,1))./norm(W_ZF_0(:,1)))^2.*(dist_BS_WS_min_0(1).*10^3).^(-alpha);% Received power at WS0 from BS0
    %% Intra-cell interference 
    Pr_I1=0;
    for ws=2:Mw 
        Pr_I1=Pr_I1+aP.*(norm(h0(1,:)*W_ZF_0(:,ws))./norm(W_ZF_0(:,ws)))^2.*(dist_BS_WS_min_0(1).*10^3).^(-alpha);% Received power at the wsth WS from BS0
    end
    %% Inter-cell interference
    FC_o = sqrt(0.5)*(randn(N_BS,Nt) + 1i*randn(N_BS,Nt));% Channels between WS0 and other BSs
    Pr_I2=0;
    for bs=1:N_BS
         if bs~=ser_ind
             h2=FC(:,:,bs);
             W_ZF=h2'/(h2*h2');
             for ws=1:Mw    
                 Pr_I2=Pr_I2+aP.*(norm(FC_o(bs,:)*W_ZF(:,ws))./norm(W_ZF(:,ws)))^2.*(dist_BS_WS0(bs).*10^3).^(-alpha);     
             end
        end
    end
    Pr_tot(iter)=Pr_d+Pr_I1+Pr_I2; %Total received power at WS0
end
%% Coverage and Wake-up Probability
PrdB_tot=10.*log10(Pr_tot);
Prob_wu=zeros(length(T),1);
for t=1:length(T)
    for i=1:NbIter
        if PrdB_tot(i) >= TdB(t)
           Prob_wu(t)=Prob_wu(t)+1;
        end
    end
end
Prob_wu=Prob_wu./(NbIter);
cd ("/MATLAB Drive/Final-result-WuR-ZF/Database")
save (['DB_WuR_',num2str(Nt),'N_',num2str(alpha*10),'alpha',num2str(Mw),'Mw_',num2str(Border),'Border_',num2str(NbIter),'R'])
cd ("/MATLAB Drive/Final-result-WuR-ZF")


