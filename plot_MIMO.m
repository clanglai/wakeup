clear; close all; clc; 
addpath("/MATLAB Drive/Final-result-WuR-ZF/Database")

figure
load('DB_WuR_19N_40alpha10Mw_2Border_10000R.mat','Prob_wu','TdB','alpha','Pt','Mw','lamda_b','Nt','NbIter','a') 
[Pw, Pw_broadcast]=get_ProbWu_anal_exact(TdB,a,alpha,Pt,lamda_b,Mw);
plot(TdB,Pw_broadcast,'r:','LineWidth',1); 
hold on
plot(TdB,Prob_wu,'k','LineWidth',1);
hold on
plot(TdB,Pw,'k*','LineWidth',1); 

load('DB_WuR_40N_40alpha10Mw_2Border_10000R.mat','Prob_wu','TdB','alpha','Pt','Mw','lamda_b','Nt','NbIter','a') 
hold on
plot(TdB,Prob_wu,'b-.','LineWidth',1.2);

load('DB_WuR_60N_40alpha10Mw_2Border_10000R.mat','Prob_wu','TdB','alpha','Pt','Mw','lamda_b','Nt','NbIter','a') 
hold on
plot(TdB,Prob_wu,'m--','LineWidth',1.2);
grid on

legend('WuR-SISO','WuR-MIMO (N_{t}=19): Monte-carlo simulations','WuR-MIMO (N_{t}=19): Analytical Analysis','WuR-MIMO (N_{t}=40): Monte-carlo simulations','WuR-MIMO (N_{t}=60): Monte-carlo simulations')
xlabel('Threshold in dB');ylabel('Wake-up Probability')


