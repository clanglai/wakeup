function [Prob_wu_ZF,Prob_broadcast]=get_ProbWu_anal_exact(TdB,a,alpha,Pt,lambda,Mw)%% Simulation parameters
%%
T = 10.^(TdB./10);
lambdax=lambda.*10^-6;
Pt
aP=a*Pt/Mw; aP
m=1;F_temp=@(t)t.^(2/alpha+Mw-m-1).*(1-t).^(-2/alpha+m-1);
Delta=(2.*pi./alpha).*(nchoosek(Mw,1).*integral(F_temp,0,1,'arrayvalued', true));

for mm=2:Mw
    Delta=Delta+(2.*pi./alpha).*(nchoosek(Mw,mm).*integral(@(t)t.^(2/alpha+Mw-mm-1).*(1-t).^(-2/alpha+mm-1),0,1,'arrayvalued', true));
end

F_a=@(x)lambdax.*(aP).^(2./alpha).*x.^(2./alpha).*Delta;

Prob_wu_ZF=integral(@(x)exp(-x.*T)./(pi.*x).*exp(-F_a(x).*cos(2.*pi./alpha)).*...
    sin(F_a(x).*sin(2.*pi./alpha)),0,Inf,'arrayvalued', true);

% code Nour to caclulate the broadcast Wake-up Probability Fig. 5 in [1]
cdf=zeros(length(TdB),1);
psi=lambdax*Pt^(2/alpha);
x=(2*pi^2*psi*a^(2/alpha))/(alpha*tan((2*pi)/alpha));
y=(2*pi^2*psi*a^(2/alpha))/(alpha);
for i=1:length(T)
    f=@(u) (1./(pi.*u)).*exp(-u.*T(i)).*exp(-x.*u.^(2/alpha)).*sin(y.*u.^(2/alpha));
    cdf(i)=1-integral(@(u)f(u),0,Inf(1));
end
cdfnum=erfc((((pi.^2).*psi)./4).*sqrt(a./T));
Prob_broadcast=1-cdf;

%KOUZAYHA, Nour, DAWY, Zaher, ANDREWS, Jeffrey G., et al. 
% Joint downlink/uplink RF wake-up solution for IoT over cellular networks. 
% IEEE Transactions on Wireless Communications, 2017, vol. 17, no 3, p. 1574-1588.